names = ["Avery", "Bob", "Val", "Jim"]

def give_higher_order_name(n1, n2):
    shorter_name_length = min(len(n1), len(n2))
    for i in range(shorter_name_length):
        print(n1[i], n2[i])
        print(ord(n1[i]), ord(n2[i]))
        if n1[i] < n2[i]:
            return n1
        elif n2[i] < n1[i]:
            return n2

for i in range(len(names) - 1):
    print(give_higher_order_name(names[i], names[i + 1]))
