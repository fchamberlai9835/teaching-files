# Parse a File with Java

First, let's write the necessary Java just to read and print out a file, line by line.

    import java.io.FileReader;
    import java.util.Scanner;

    public class FileParser {
    	public static void main(String args[]) {
    		try {
    			FileReader fin = new FileReader("data.txt");
    			Scanner src = new Scanner(fin);
    			String activeLine;
    			while (src.hasNext()) {
    				activeLine = src.nextLine();
    				System.out.println(activeLine);
    			}
    		} catch(Exception e) {
    			System.out.println(e);
    		}
    	}
    }

Now, we can add regex to read the first letter. The regex objects have a few more steps, but the basic concept is the same: use regex to search through text. Also, we can see how to label a capture group in Java.

	import java.io.FileReader;
	import java.util.Scanner;
	import java.util.regex.Pattern; // added line
	import java.util.regex.Matcher; // added line
	 
	 public class FileParser {
	 	public static void main(String args[]) {
			Pattern studentPattern = Pattern.compile("^(?<firstLetter>[a-zA-Z])"); // added line
	 		try {
	 			FileReader fin = new FileReader("data.txt");
	 			Scanner src = new Scanner(fin);
	 			String activeLine;
				Matcher studentMatcher; // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
					System.out.println(activeLine); // deleted line
					studentMatcher = studentPattern.matcher(activeLine); // added line
					studentMatcher.find(); // added line
					System.out.println(studentMatcher.group("firstLetter")); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);

We can get the entire line, and organize all the information.

	public class FileParser {
	 	public static void main(String args[]) {
			Pattern studentPattern = Pattern.compile("^(?<firstLetter>[a-zA-Z])"); // deleted line
			Pattern studentPattern = Pattern.compile("^(?<firstName>[A-Za-z]+)\\s(?<lastName>[A-Za-z]+)\\s+(?<age>\\d+)\\s+(?<cards>\\d+)$"); // added line
	 		try {
	 			FileReader fin = new FileReader("data.txt");
	 			Scanner src = new Scanner(fin);
	 			String activeLine;
	 			Matcher studentMatcher;
				src.nextLine(); // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
	 				studentMatcher = studentPattern.matcher(activeLine);
	 				studentMatcher.find();
					System.out.println(studentMatcher.group("firstLetter")); // deleted line
					System.out.print(studentMatcher.group("firstName")); // added line
					System.out.print(studentMatcher.group("lastName")); // added line
					System.out.print(studentMatcher.group("age")); // added line
					System.out.println(studentMatcher.group("cards")); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);

We can use `String.format` to organize our information a little bit better.

	public class FileParser {
	 			String activeLine;
	 			Matcher studentMatcher;
	 			src.nextLine();
				String outputString; // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
	 				studentMatcher = studentPattern.matcher(activeLine);
	 				studentMatcher.find();
					System.out.print(studentMatcher.group("firstName")); // deleted line
					System.out.print(studentMatcher.group("lastName")); // deleted line
					System.out.print(studentMatcher.group("age")); // deleted line
					System.out.println(studentMatcher.group("cards")); // deleted line
					outputString = String.format("%s %s ", studentMatcher.group("firstName"), studentMatcher.group("lastName")); // added line
					outputString += String.format("is %s years old ", studentMatcher.group("age")); // added line
					outputString += String.format("and has %s pokemon cards.", studentMatcher.group("cards")); // added line
					System.out.println(outputString); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);

It's okay if you don't know regex off the top of your head. Whenever I have a regex problem, I load up both [regexone](regexone.com) and [regex101](regex101.com) and review regex before trying to solve a problem.
