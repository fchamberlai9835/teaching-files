# Parse a file with Python and Regex

Before you do this, you should complete the challenges at [regexone](regexone.com). If you're ever need to debug some regex, I would reccomend doing that at [regex101](regex101.com).

We will be using a file for these examples, `data.txt`. I recommend you familiarize yourself with it beforehand.

## Goal 1: Open a file in Python

Let's start with opening a file in Python. This syntax for iterating over a file is pretty nice.

	with open("data.txt", "r") as f:
		for line in f.readlines():
			print(line)

## Goal 2: Open a file and grab the first letter

Python has regex specific strings. Kinda like how f-strings look like `f''` and bytestrings look like `b''`, regex strings look like `r''`. We are going to use the regex character `^` to indicate that we're looking at the beginning of a line, and the regex character `.` to indicate that we're looking for only one thing, but it can be anything.

	import re # added line
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			print(line) # deleted line
			one_match = re.match(r'^.', line) # added line
			print(one_match.group()) # added line

`group` lets us see the entirity of what we matched. Below, I am using regex to get four characters (by using four `.` regex characters), I am starting with `^` to show that I want to start at the beginning of the line, and I have parentheses around the first dot and the fourth dot, those dots will be my "capture groups." `groups` will give me the parts in parentheses, and `group` will give me the entire match.
	 
	 with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^.', line) # deleted line
			print(one_match.group()) # deleted line
			one_match = re.match(r'^(.)..(.)', line) # added line
			print(one_match.groups(), one_match.group()) # added line

Instead of grabbing the first and fourth character, we can grab the first and second name. The `[A-Za-z]` means any uppercase or lowercase letters, and the following `+` means to look for one or more characters that meet the `[A-Za-z]` condition in a row. So that will continue grabbing characters to the right, until it comes to punctuation, or a digit, or whitespace. `\s` catches one unit of any whitespace. And then we repeat the previous syntax to grab the last name.

	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines():
			one_match = re.match(r'^(.)..(.)', line) # deleted line
			one_match = re.match(r'^([A-Za-z]+)\s([A-Za-z]+)', line) # added line
	 		print(one_match.groups(), one_match.group())

Our parentheses indicate the special areas that we want captured, but we can make it clearer. We can name these `capture groups` using the syntax `(?P<sample_name>)`. The syntax will be slightly different in Java, it will look like `(?<sampleName>)`. Then, once we've given these capture groups names, we can select them by name.

	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^([A-Za-z]+)\s([A-Za-z]+)', line) # deleted line
			print(one_match.groups(), one_match.group()) # deleted line
			one_match = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)', line) # added line
			print(one_match.group('first_name'), one_match.group('last_name')) # added line

We can grab ages as well, the `\d+` will grab one or more digits. Unfortunately, this is where my file started to error out.

	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)', line) # deleted line
			print(one_match.group('first_name'), one_match.group('last_name')) # deleted line
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line) # added line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.") # added line

Our regex fails to filter the first line, because the first line is just headers. Fortunately, we can just skip over this line. `f.readlines()` just gives a list, so we can just slice that out.

	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines(): # deleted line
		for line in f.readlines()[1:]: # added line
	 		matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line)
	 		print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.")

And we can grab the entire line. The last new character we are seeing here is the `$`, which indicates that this is looking at the end of the line. If there were an additional character 'a' after someone's number of pokemon cards in `data.txt`, then our program would error out.

	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines()[1:]:
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line) # deleted line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.") # deleted line
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)\s+(?P<cards>\d+)$', line) # added line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old,", end=' ') # added line
			print(f"and they have {matches.group('cards')} pokemon cards.") # added line

This program is good, but Python is really best when you avoid loading large sets of data into memory. Instead of putting everything into a list all at once, we can step through it line by line, without forcing Python to look at the entire file all at once.

We have the additional `f.readline()` at the beginning to skip over the first line.

	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines()[1:]: # deleted line
		f.readline() # added line
		while True: # added line
			line = f.readline() # added line
			if line is None or not line: # added line
				break # added line
	 		matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)\s+(?P<cards>\d+)$', line)
	 		print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old,", end=' ')
	 		print(f"and they have {matches.group('cards')} pokemon cards.")

Now let's learn how to do this in Java.
