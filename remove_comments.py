in_md = 'bubble.md'
out_md = 'bubble_changed.md'
with open(in_md, 'r') as bubble_file:
    with open(out_md, 'w') as bubble_file_output:
        for line in bubble_file:
            bubble_file_output.write(line.replace('# added line', '').replace('# deleted line', ''))
