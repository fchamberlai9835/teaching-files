# Parse a File with Java

Students will go to [regexone](regexone.com) and [regex101](regex101.com)

    import java.io.FileReader;
    import java.util.Scanner;

    public class FileParser {
    	public static void main(String args[]) {
    		try {
    			FileReader fin = new FileReader("data.txt");
    			Scanner src = new Scanner(fin);
    			String activeLine;
    			while (src.hasNext()) {
    				activeLine = src.nextLine();
    				System.out.println(activeLine);
    			}
    		} catch(Exception e) {
    			System.out.println(e);
    		}
    	}
    }






	import java.io.FileReader;
	import java.util.Scanner;
	import java.util.regex.Pattern; // added line
	import java.util.regex.Matcher; // added line
	 
	 public class FileParser {
	 	public static void main(String args[]) {
			Pattern studentPattern = Pattern.compile("^(?<firstLetter>[a-zA-Z])"); // added line
	 		try {
	 			FileReader fin = new FileReader("data.txt");
	 			Scanner src = new Scanner(fin);
	 			String activeLine;
				Matcher studentMatcher; // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
					System.out.println(activeLine); // deleted line
					studentMatcher = studentPattern.matcher(activeLine); // added line
					studentMatcher.find(); // added line
					System.out.println(studentMatcher.group("firstLetter")); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);










	public class FileParser {
	 	public static void main(String args[]) {
			Pattern studentPattern = Pattern.compile("^(?<firstLetter>[a-zA-Z])"); // deleted line
			Pattern studentPattern = Pattern.compile("^(?<firstName>[A-Za-z]+)\\s(?<lastName>[A-Za-z]+)\\s+(?<age>\\d+)\\s+(?<cards>\\d+)$"); // added line
	 		try {
	 			FileReader fin = new FileReader("data.txt");
	 			Scanner src = new Scanner(fin);
	 			String activeLine;
	 			Matcher studentMatcher;
				src.nextLine(); // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
	 				studentMatcher = studentPattern.matcher(activeLine);
	 				studentMatcher.find();
					System.out.println(studentMatcher.group("firstLetter")); // deleted line
					System.out.print(studentMatcher.group("firstName")); // added line
					System.out.print(studentMatcher.group("lastName")); // added line
					System.out.print(studentMatcher.group("age")); // added line
					System.out.println(studentMatcher.group("cards")); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);







	public class FileParser {
	 			String activeLine;
	 			Matcher studentMatcher;
	 			src.nextLine();
				String outputString; // added line
	 			while (src.hasNext()) {
	 				activeLine = src.nextLine();
	 				studentMatcher = studentPattern.matcher(activeLine);
	 				studentMatcher.find();
					System.out.print(studentMatcher.group("firstName")); // deleted line
					System.out.print(studentMatcher.group("lastName")); // deleted line
					System.out.print(studentMatcher.group("age")); // deleted line
					System.out.println(studentMatcher.group("cards")); // deleted line
					outputString = String.format("%s %s ", studentMatcher.group("firstName"), studentMatcher.group("lastName")); // added line
					outputString += String.format("is %s years old ", studentMatcher.group("age")); // added line
					outputString += String.format("and has %s pokemon cards.", studentMatcher.group("cards")); // added line
					System.out.println(outputString); // added line
	 			}
	 		} catch(Exception e) {
	 			System.out.println(e);
