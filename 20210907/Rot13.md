# Lab: Rot13

Let's make a file that will take in a string, and rotate the letters by 13 places. This will both encode and decode, since rotating twice by 13 is the same as rotating once by 26, which is the same number as the number of letters in the alphabet.

| Index   | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|---------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| English | a| b| c| d| e| f| g| h| i| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| y| z|
| ROT+13  | n| o| p| q| r| s| t| u| v| w| x| y| z| a| b| c| d| e| f| g| h| i| j| k| l| m|

    Rot.Encrypt("hello") -> "uryyb"

HINT: You will need to translate between numbers and letters. Here's some relavent python code:

    >>> letter_list
    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    >>> unrotated = letter_list.index('s')
    >>> print(unrotated)
    18
    >>> rotated = unrotated + 1
    >>> print(rotated)
    19
    >>> letter_list[rotated]
    't'

## Bonus Goal

Allow the method to take in a number as well, and rotate the letters by that number instead of 13
