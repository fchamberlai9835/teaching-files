# Guide: Sleep Judger

In this guide, we will make a program that judges whether or not you've gotten enough sleep.

The first step is to create our file, `SleepJudger.java`. Inside that file we can create a class with the same name as the file, `SleepJudger`.


    public class SleepJudger { // new line

           public static void main(String[] args) { // new line
           } // new line

    } // new line

When I add new lines to a file, I like to add the comment `// new line`. You don't have to copy that comment.

On the first line we have the syntax to create a `public class`. On the second line, we have the syntax to create a `public method` that Java will run if you choose to run the file. `public` means that it can be accessed by any other class. `static` means that it doesn't have direct access to the class. And `void` means that it won't return anything.

It's okay if all this doesn't make sense immediately.

Now we can add a new method that takes in an integer, and responds by judging if it's enough sleep.

    public class SleepJudger {

    	public void judgeSleepAmount(int numberOfHours) { // new line
    		if (numberOfHours < 7) { // new line
    			System.out.print("Only "); // new line
    			System.out.print(numberOfHours); // new line
    			System.out.println(", that's not enough!"); // new line
    		} // new line
    	} // new line

    	public static void main(String[] args) {
    	}

    }

Now we've added a method that takes in an integer `numberOfHours`, and if that number is too small it responds by announcing that someone didn't get enough sleep.

Unfortunately, currently we aren't testing our method. We can test it in the `static main` method, the method that will run by default if we run our file. Because the `static main` method is `static`, it won't have access to a class with the method unless we create an instance of that class.

    public class SleepJudger {

    	public void judgeSleepAmount(int numberOfHours) {
    		if (numberOfHours < 7) {
    			System.out.print("Only ");
    			System.out.print(numberOfHours);
    			System.out.println(", that's not enough!");
    		}
    	}

    	public static void main(String[] args) {
    		SleepJudger mySleepJudger = new SleepJudger(); // new line
    	}

    }

And now that we have created an instance of the class `SleepJudger`. We can use this instance to test out our method.


    public class SleepJudger {

    	public void judgeSleepAmount(int numberOfHours) {
    		if (numberOfHours < 7) {
    			System.out.print("Only ");
    			System.out.print(numberOfHours);
    			System.out.println(", that's not enough!");
    		}
    	}

    	public static void main(String[] args) {
    		SleepJudger mySleepJudger = new SleepJudger();
    		mySleepJudger.judgeSleepAmount(6); // new line
    	}

    }

Now we should be able to run our program, and see it print a response to the number of hours we put in.

Once we've tested that, we can add more to the `judgeSleepAmount` method:

    public class SleepJudger {

           public void judgeSleepAmount(int numberOfHours) {
                   if (numberOfHours < 7) {
                           System.out.print("Only ");
                           System.out.print(numberOfHours);
                           System.out.println(", that's not enough!");
                   } else if (numberOfHours > 11) { // new line
                           System.out.print(numberOfHours); // new line
                           System.out.println(" hours, that's too many!"); // new line
                   } else { // new line
                           System.out.println("That sounds good!"); // new line
                   } // new line
           }

           public static void main(String[] args) {
    ...

Now, inside the `main` method you can test different numbers of hours to see the different responses your program outputs.
